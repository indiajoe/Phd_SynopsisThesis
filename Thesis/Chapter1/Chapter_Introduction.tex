% Chapter Template

\chapter{Introduction} % Main chapter title

\label{Chapter_Introduction} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%Once up on a time, in a giant molecular cloud far far away...
The origin of our solar system has fascinated humanity from ancient times. Modern rigorous scientific studies to understand the formation of planets can be traced back to three centuries ago when Baffon (1749) first suggested that planets were formed via condensation of materials knocked out of the Sun by colliding comets. The current, well established theory of star and planet formation from the collapse of nebular clouds  (`the nebular hypothesis') can be traced back to philosopher Immanuel Kant in 1755. It was Laplace, in 1796, who formalised this nebular hypothesis of the formation of a disc by the collapse of giant nebular clouds. He also refuted Baffon's idea, by arguing that if planets were formed by matter knocked out of the  Sun, those material's Keplerian orbit would most likely crash them back in to the Sun. However, Laplace's nebular hypothesis could not explain why the angular momentum of the Sun, which consists of 99.8\% mass of our solar system, has only 1\% of the total angular momentum of our solar system\footnote{Even with the modern theories of angular momentum transport in protoplanetary discs, angular momentum still haunts our theories in more subtle ways like it did to Laplace three centuries ago!}. Hence, the nebular hypothesis lost its popularity and was replaced by other competing models of two body interactions (which were in the same spirit of Baffon's theory). In the early half of the 20th century, various models in which material pulled out tidally  from the Sun by other flyby stars, condense into planets were put forth separately by Chamberlin, Moulton and Jeans (1900s-20s). By the 1930s, many astronomers, mainly Jeffreys, Russel, and Spitzer, identified various issues with these models. For instance, the low probability of flybys by other stars, the difficulty in attaining large orbits for outer planets, and the inability of the hot gas to collapse into planets (ironically Spitzer used Jean's equilibrium criterion itself to demonstrate this!). The nebular hypothesis finally made a come back in the 1960s -70s in the form of protoplanetary disc models by the works of Lyttleton, McCrea, Safranov, etc. (see \citet{woolfson93} for a detailed historical summary).

Currently, it is well established that stars and planetary systems form by fragmentation and gravitational collapse of giant molecular clouds. The conservation of initial net angular momentum of the collapsing clouds in these stellar nurseries inevitably results in formation of a rotating accretion disc around the protostars\footnote{While it is also likely the case in high mass ($>$2 \msun) stars, due to relatively poor observational evidence compared to low mass stars, the last word on the question of formation of accretion disc in high mass stars is not yet out.}.  With the advent of space-based infrared telescopes and ground-based sub-millimetre antennae over the last 3 decades, astronomers have revolutionised the understanding of this crucial baby-stage of star and planet formation in deep stellar molecular cloud nurseries.

\section{Life of a protoplanetary disc}
The formation timescale of a protoplanetary disc is relatively very short ($\sim$10$^4$ years). It is a function of the rotation rate of the central core as well as the free-fall time \citep{terebey84}. Since these parameters are  sensitive to ambient conditions like the magnetic field strength, initial angular momentum, etc., it is quite likely that the initial masses and sizes of protoplanetary discs are spread over a large range of values.
The heavily embedded initial cocoon phase, classified as Class 0, is not seen in optical or near-infrared wavelengths. Their observational studies started only recently after the development of technologies in the early 90s to probe them in the far-infrared and millimeter wavelengths.
After Class 0, the protostar moves to Class I, Class II, and Class III stages\footnote{Between Class I and Class II, there is also an intermediate class of sources known as flat spectrum sources. Their slope of mid-infrared SED falls between Class I and Class II.} before entering the main-sequence life. They are classified based on the slope of spectral energy distribution (SED) in mid-infrared regime  \citep{lada87,greene94}. Class I stage typically lasts for about $\sim$ 0.5 Myr. About 50\% of the final main-sequence mass of the central star is accumulated by the early phase of Class I stage ($\sim$0.1 Myr). By the time the protostar with its circumstellar disc system enters the Class II stage, they slowly clear their  circumstellar envelope cocoons and start becoming visible in the optical wavelengths. Meanwhile, the accretion of matter from the disc to the central star continues for the lifetime of the protoplanetary disc, which varies from 2 to 10 Myr (with a median of around 3 Myr). As the star becomes a Class III source, the accretion rates drop significantly (typically classified as weak-line T-Tauri stars). And finally, at a certain epoch, the disc suddenly vanishes in a short timescale. Discs in this clearing process are called transitional discs. The presence of mid-infrared flux excess at this stage, even when we do not see any significant near-infrared excess implies, the disc vanishes inside-out from the central source\footnote{By mechanisms like photo-evaporation.} (see review by \citet{williams11} and references within). Figure \ref{img:starformation} shows the summary of the formation stages of a protoplanetary disc system.
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.8\textwidth]{\ChapDir/ttauri_formation.jpg} 
\end{center}
\caption[Stages in the formation of a star-planetary system]{Stages and timescales in the formation of a star-planetary system from the collapse of giant molecular clouds. Image Courtesy: Thomas Greene, American Scientist, Jul-Aug 2001 }
\label{img:starformation}
\end{figure}

All the processes of planet formation and the growth of a central protostar have to finish in this short period of circumstellar accretion disc's existence (median lifetime of $\sim$3 Myr). The physical properties and the thermal state of the protoplanetary disc during its short period of existence determine the environment in which planets and stellar systems have to form. There is a complex radiative feedback in play here due to the self irradiation of the released accretion energy. This heating significantly affects the thermal and gravitational stability of the accretion disc, which in turn influences the accretion rate.

\section{Hints for episodic accretion}
The crucial environmental parameters like the chemical composition, gravitational stability, thermal state, etc., of the protoplanetary disc are governed by the gravitational energy released from accretion. Hence, understanding the evolution of the  accretion rate during the lifetime of a protoplanetary disc is imperative.
\subsection{Luminosity problem}
As mentioned in the previous section, half of the stellar mass is accreted in the first $\sim$ 0.1 Myr. It implies that, for a 1 \msun star, the average accretion rate during that period should be $\sim$ 5 $\times$ 10$^{-6}$ \msun yr$^{-1}$, which will correspond to a mean accretion luminosity of about 25 \lsun. This is much larger than the average observed luminosities of protostars from far-infrared surveys \citep{dunham08}. This famous problem in star formation is known as the ``Luminosity problem" \citep{kenyon90,evans09}. Even though the measurement of the accretion rates from the envelope to disc is not that robust, there is no significant indication of a mismatch between the expected and the observed accretion rates from the envelope to disc. This open problem can be solved if we envisage the hypothesis that the accretion from the disc does not occur at a constant rate, instead, it occurs in intermediate short period bursts of high accretion events. Since most of the sources we observe in a survey are statistically likely to be between these accretion bursts, we would possibly detect only the lower rates of accretion \citep{hartmann96}.

\subsection{Calcium-aluminum-rich inclusions \& Chondrules}
In our own solar system, the thermal history of our protoplanetary disc is preserved in the chondrite meteorites. They contain complex millimeter to centimeter sized objects called calcium-aluminum-rich inclusions (CAIs) and Chondrules (see Figure \ref{img:AllendeMeteorite}). Radio dating of these objects show that they were formed recurrently during the lifetime of our historical protoplaneatry disc. CAIs can be formed only in high-temperature environments ($>$1300 K) and show signatures of recurrent re-melting. Chondrules were formed by getting flash heated to melting point and slowly cooled at slightly lower temperatures ($<$1000 K), recurrently over a timescale of  $\sim$ 3 Myr \citep{connelly12}. These temperatures and timescales are easily achieved in the inner part of the disc if the episodic accretion outburst hypothesis is true. Chondrules and CAIs once formed at the inner parts of the disc could get sprayed all over the outer regions of the disc via outflows \citep{shu96}, or/and via radial diffusion along the midplane of the disc \citep{ciesla07}. 
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.5\textwidth]{\ChapDir/Allende_meteorite_labelled.png} 
\end{center}
\caption[Allende meteorite which has CAIs and Chondrules embedded in it]{A slice of famous chondrite Allende meteorite which has CAIs and Chondrules embedded in it.  Courtesy: Museum of Natural History, NY, USA}
\label{img:AllendeMeteorite}
\end{figure}


\subsection{Outflows and the associated structures}
Young stellar objects (YSOs) are also drivers of the spectacular, heavily collimated outflows and jets seen in star-forming regions (see Figure \ref{img:ouflowHH212img}). When matter is accreted from the accretion disc, it also transfers angular momentum to the central star. If the angular momentum is not taken away from the system, a typical accreting classical T-Tauri star (CTTS) will reach break-up speed within the timescale of a million year.
However, observationally CTTSs are found to be rotating only at $\sim$10\% of the break-up speed \citep{herbst07}. Their rotation rate is also found to be almost constant throughout their disc accretion phase spanning a few million years \citep{irwin09}. Outflows from these objects are the most efficient way to take away the angular momentum from the star-disc system and prevent the star from spinning up. Hence, we expect the outflow to be proportional to the accretion rate.
If that is the case, then one way to explain the dense knots seen in the outflows (see Figure \ref{img:ouflowHH212img}) are episodic events of high accretion. They could be the fossil evidence of accretion outbursts in the YSO's history \citep{dopita78,reipurth97,ioannidis12}.
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.4\textwidth]{\ChapDir/hh212_jet_mjm_stanke.jpg} 
\end{center}
\caption[Near-infrared image of a heavily collimated bipolar outflow, HH212]{Near-infrared image of a spectacular, heavily collimated bipolar outflow, HH212. Knots are seen along the outflow. The central protostar driving the outflow is hidden by the disc due to its edge on view. Image Courtesy: Zinnecker, A. et al. 1997, Thomas Stanke, ESO}
\label{img:ouflowHH212img}
\end{figure}

\subsection{FUors and EXors Phenomenon}
Observationally, some YSOs have been serendipitously discovered to undergo a sudden increase in brightness. Traditionally, on the basis of the light curve and spectrum, these accretion outbursts are classified as FUors (showing decades-long outbursts with 4--5 mag change in optical bands, and an absorption-line spectrum) and EXors (showing outbursts of a few months to years long with 2--3 mag change in optical bands, and an emission-line spectrum) \citep{herbig77,hartmann96, hartmann98}. Figure \ref{img:classicalFUors_HartmannPlot}a shows the light curves of the three classical FUors, namely FU Ori, V1515 Cyg, and V1057 Cyg. 

\begin{figure}
  \centering
  \begin{tabular}[b]{@{}p{0.45\textwidth}@{}}
    \centering\includegraphics[width=\linewidth]{\ChapDir/3_FUOri_Bmag-Hartmann-book.png} \\
    \centering\small (a) 
  \end{tabular}%
  \quad
  \begin{tabular}[b]{@{}p{0.5\textwidth}@{}}
    \centering\includegraphics[width=\linewidth]{\ChapDir/EpisodicAccretionHartmann.jpg} \\
    \centering\small (b)
  \end{tabular}
  \caption[Light curves of classical FUors \& episodic accretion hypothesis]{a) Light curves of the three classical FUors (FU Ori, V1515 Cyg, and V1057 Cyg) showing a rapid rise to outburst by undergoing an increase in their optical magnitude by $\sim$5 mag. b) The plot shows the episodic accretion hypothesis during the formation of a YSO. The YSO will spend most of its time in quiescent phase, intermediately erupting to short duration accretion outbursts (FUors or EXors) multiple times in its pre-main sequence lifetime.  Image Courtesy: \citep{hartmann09} }
  \label{img:classicalFUors_HartmannPlot}
\end{figure}



%\begin{figure}[h]
%\begin{center}
%\includegraphics[width=0.5\textwidth]{\ChapDir/3_FUOri_Bmag-Hartmann-book.png} 
%\end{center}
%\caption{Light curves of the three classical FUors (FU Ori, V1515 Cyg, and V1057 Cyg) showing a rapid rise to outburst by undergoing an increase in their optical magnitude by $\sim$5 mag.  Image Courtesy: \citep{hartmann09}}
%\label{img:classicalFUors_HartmannPlot}
%\end{figure}

These sources were all spatially and kinematically associated with star forming regions. They showed reflection nebulae, significant infrared excess, CO band head absorptions in near infrared, different spectral types between optical and near infrared bands, etc. The widths of the emission lines as a function of wavelength were consistent with what we would expect from a bright accretion disc with a radial temperature gradient. These properties confirmed FUors to be YSOs with a luminous accretion disc, undergoing heavy rates of accretion \citep{hartmann98}. 
When FUors (EXors) undergo an outburst, its accretion rate increases by a factor of $\sim$ 100 ($\sim$ 10), and hence, they are ideal candidates for the episodic outbursts which could possibly solve all the open issues mentioned in previous subsections. Figure \ref{img:classicalFUors_HartmannPlot}b shows the overall picture of this episodic accretion outburst hypothesis in the accretion history of a YSO.
Due to the short timescales of outbursts in comparison to the millions-of-years timescale of star formation, these events are extremely rare, and less than two dozen confirmed FUor and EXor outbursts have been discovered so far \citep{audard14}.
%\begin{figure}[h]
%\begin{center}
%\includegraphics[width=0.5\textwidth]{\ChapDir/EpisodicAccretionHartmann.jpg} 
%\end{center}
%\caption{The episodic accretion hypothesis in the formation of a YSO. The YSO spends most of its time in quiescent phase, intermediately erupting to FUors or EXors accretion outbursts for a short duration multiple times in its pre-main sequence lifetime.  Image Courtesy: \citep{hartmann09}}
%\label{img:FUorsAccretiongraph}
%\end{figure}

\section{Implications of episodic accretion}
\subsection*{Motivation of This Thesis}
As described in the previous section, the episodic accretion hypothesis can potentially solve the open issues in protoplanetary disc accretion. It will also have very significant implications in the star, planet, and comet formation as well. By comparing the silicate emission line before and after the outburst phase of EX Lup (a classical EX Or object), \citet{abraham09} showed the outburst event  resulted in silicate crystallization. This shows the importance of this phenomenon in planet and comet formation. It is worthwhile to mention here that the formation of crystallised silicate in comets of our solar system is another poorly understood phenomenon. Thermal heating during the accretion events can make the protoplanetary discs stable from gravitational instabilities\footnote{Condition for gravitational instability is given by the Toomre $Q$ parameter being less than unity. $Q(R) = c \Omega/\pi G \Sigma$ , where $c$ is the speed of sound (proportional to square root of the temperature), $\Omega$ is the orbital angular velocity, and $\Sigma$ is the disc surface density. Hence, at high temperatures the radius upto which the disc is gravitationally stable expands.}, which could negatively affect the fragmentation and formation of giant gas planets \citep{stamatellos11}. Another crucial factor which directly affects the planet formation is the expansion of the snow-lines during an accretion outburst. Planets are formed by grains getting stuck together and growing in size when they collide. An ice mantle (H$_2$O or CO ice) around the grains can make them sticky. Snow-line is the radius at which the temperature of the disc drops below the sublimation point of the corresponding ice. Only grains beyond this radius will have a significant ice mantle around them. Episodic accretion events in solar type stars can result in expansion of the snow-line radius from $\sim$5 AU to $\sim$40 AU, negatively affecting the formation of rocky planets within that radius \citep{cieza16}. Episodic accretion can also significantly change the pre-main-sequence isochrones in a  Hertzsprung-Russell (HR) diagram, which is used extensively for initial mass function studies and age/mass estimation of YSOs \citep{hartmann11,hosokawa11,baraffe12}. From the number statistics of the discovered outbursts, it is estimated that every low-mass star undergoes $\sim$50 such short-duration outbursts during its formation stage \citep{scholz13}. Hence, it is fundamentally necessary to understand and study the environment into which a protoplanetary disc system gets transformed to during accretion outbursts. 

In Chapters \ref{Chapter_V899Mon} and \ref{Chapter_V1647Ori} of this thesis, I present our results from the observational study of the environmental changes we discovered in two such outburst sources. 
The physical mechanism which is driving these episodic accretion events is not yet fully understood. In Chapter \ref{Chapter_OutburstModel}, I present the constraints we obtained which enabled us to rule out certain types of outburst mechanism models. We therefore proposed an observationally consistent mechanism for explaining the features of the light curves that we observed. The short timescale variations in the accretion rate of FUors and EXors make them unique laboratories to monitor the simultaneous evolution of the outflow with respect to the accretion rate. This enabled us to constrain, in Chapter \ref{Chapter_OutflowModels}, the outflow mechanism under way  in these objects. And finally, the most crucial parameter for quantifying the effect of these outbursts on planet and star formation is their occurrence frequency. In Chapter \ref{Chapter_BayesianModel}, I present a hierarchical Bayesian model I developed for constraining, as well as comparing the outburst frequency across different classes of YSOs.

\section{Monitoring of FUors and EXors (MFES) Program}
Recognising the importance of the rare FUors and EXors phenomena in probing the environment and evolution of the protoplanetary discs, we started a long-term multi-wavelength monitoring program of all confirmed FUors and EXors. During the initial phase we had continuous monitoring of only two sources. We expanded this list  significantly to include all observable northern hemisphere sources in 2008 (see Appendix \ref{Appendix_MFES} for the complete list of sources). During the period 2008 to 2016, we have observed for about 175 nights for this program using various optical and near-infrared telescopes. Details of observations and the data reduction procedures are discussed in Chapter \ref{Chapter_Obs}.

For this thesis work, two of the most peculiar sources from our list, V899 Mon and V1647 Ori were selected for the detailed analysis. Results presented in this thesis are from these two sources.

\section{Instrumentation  (Part II)}
In the \textsc{MFES} program, a significant fraction of our sources were heavily embedded, and we needed near-infrared observations to capture the photons escaping from the heavily extinct regions like the inner parts of the accretion disc and the magnetosphere. Part II of this thesis is on the near-infrared instrumentation work that I carried out to develop the instruments we used for this study. Chapter \ref{Chapter_tirspec} contains the summary of the personal contributions to the development, repairs, upgrades, and final calibration of the TIFR Near Infrared
Spectrometer and Imager (TIRSPEC). Chapter \ref{Chapter_TFIRBT} describes my contributions to TIFR Near Infrared Imaging Camera -II (TIRCAM2) and 100 cm TIFR Far infrared Balloon-borne Telescope. Since the  \textsc{MFES} program involved hundreds of nights of data from a wide variety of optical and near-infrared instruments, a few unified pipelines for all the instruments used in the \textsc{MFES} program were also  developed. These pipelines are designed to be general enough for any kind of science observations and are easily adaptable for many similar instruments. In Chapter \ref{Chapter_Pipelines}, the details and architecture of these data reduction pipelines are outlined. 

\vspace{1cm}

Finally, the major results of the work presented in this thesis are summarised in Chapter \ref{Chapter_Conclusion}.
