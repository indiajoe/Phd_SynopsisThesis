% Chapter Template

\chapter{Data Pipelines} % Main chapter title

\label{Chapter_Pipelines} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

% About why I needed pipleine for MFES program
The observations for our long-term \textsc{MFES} program were spread over hundreds of nights using various optical as well as near-infrared instruments (see Chapter \ref{Chapter_Obs}). For studying the long-term time variability of our sources, it was important to have a consistent reduction procedure for data from all the nights. Over a period, an automated pipeline also reduces the time required to be spent on  data reduction for doing science.

% The guidelines
Since the quality of reduction is crucial for our science, the key philosophy in the design of the pipeline is not to compromise on the quality of data reduction, even if that means compromising on the ability of full scale automation. For instance, the complex, yet crucial step of visual inspection of raw data to discard anything unusual has not been automated. On the other hand, reducing human inputs is vital for making the code less susceptible to human errors, and to save time via automation. Hence, the pipeline was designed to minimise the human input whenever we have a good algorithm without compromising on the reduction quality.

The pipeline was also designed to be general enough for use by other users of the instrument. As a by-product of this work, I released a general pipeline for all these instruments for use of the general astronomy community. In order to reduce the overheads in learning reduction of data from new instruments, while switching between multiple instruments, the interface of the pipelines was designed to be similar. By re-factoring the implementation of fundamental data reduction procedures to be independent of the instrument, we isolated the instrument specific processing steps from the common fundamental steps. Thus, multiple instruments could be incorporated into the same pipeline.
In the following sections, I outline the basic steps in the near-infrared and optical data reduction pipelines that were developed. 

\section{TIRSPEC Pipeline}\label{sec:TIRSPECpipeline}
The TIRSPEC, mounted on the side port of the 2 m HCT, is a near-infrared spectrometer and imager (see Chapter \ref{Chapter_tirspec}). TIRSPEC and the telescope are controlled remotely by the observer from The Centre for Research \& Education in Science \& Technology (CREST), Hosakote, IIA, Bangalore (India) via a satellite link. In one typical night of observation, the SUTR\footnote{Sample up-the-ramp} data acquisition mode in which TIRSPEC is operated, can generate upto 50 GB of raw data. With the existing satellite link bandwidth, it is not possible to download all the raw data to the control room in Bangalore on the same day. Hence, we had to separate our TIRSPEC data reduction pipeline into two parts. The first part, which generates the dark subtracted slope images, is run automatically at the HCT site itself, on the TIRSPEC server. The second part, which does the remaining data reduction, is run by the instrument user.

\subsection{Slope image generation}
The slope image generation is the first part of the pipeline which 
%The first part of the data reduction tool, which does corresponding readout's dark subtraction, intelligent pixel masking, and slope fitting  
is run on the TIRSPEC computer server at Hanle. Figure \ref{fig:SlopeImageGenerationPip} shows the block diagram of the stages in this part of the pipeline. It logs file creation, generates cosmic ray (CR) hit healed dark averages,  does intelligent pixel masking to recover from saturation or reset anomaly, dark subtraction and final slope fitting (see Section \ref{sec:ReadoutAndImgGen} for more details on these algorithms). This fully automated stage is run at the end of each night's observation. The data that is received by the observer at the Bangalore control room contains the dark-subtracted slope images. Two log files which are generated are also provided to the observer. One contains the log messages of the image generation pipeline, and other contains a summary of all observations done during that night.
 
 \begin{figure}[h]
\begin{center}
\includegraphics[width=0.7\textwidth]{\ChapDir/SlopeImageGenerationPipeline.png} 
\end{center}
\caption[Block diagram: Part 1 of TIRSPEC pipeline]{Block diagram of the Slope image generation stage of the TIRSPEC pipeline. This part is fully automated and runs on the TIRSPEC server at Hanle.}
\label{fig:SlopeImageGenerationPip}
\end{figure}

This part of the pipeline also includes various routines for non-linearity correction, CR hit removal, etc. If the user wants to work on the raw SUTR data and reprocess it, the modular design ensures the same code can be imported as a module in an interactive Python terminal to separately use the collection of routines. Documentation is incorporated inside the source code itself.

\subsection{Near-infrared Photometry \& Spectroscopy}
\label{sec:TIRSPECPhotoSpecProcedure}
The second part of the pipeline which does photometry or spectroscopy data reduction is run by the user. This stage is controlled by a configuration file, which the user can configure to execute a photometry or a spectroscopy pipeline. Figure \ref{fig:TIRSPECPhotoSpecPip} shows the block diagrams of this second stage of the pipeline. The first five blocks which include steps like identifying the targets, visual inspection of data quality, combining of the same dither positions (which are automatically identified), flat correction, and bad pixel interpolation are common to both the photometry and spectroscopy pipelines. In Figure \ref{fig:TIRSPECPhotoSpecPip}, the blocks which have a human face smiley represent blocks that need some human supervision.
All the blocks are independent and can also be run separately. The meta data for the pipeline is passed on from one block to the next block via standard human readable text files. This provides more control on the flow of the pipeline to advanced users, if they want to interrupt and intervene in the execution.

\begin{figure}
  \centering
  \begin{tabular}[b]{@{}p{0.6\textwidth}@{}}
    \centering\includegraphics[width=\linewidth]{\ChapDir/PhotometryPipeline.png} \\
    \centering\small (a) Photometry
  \end{tabular}%
  \quad
  \begin{tabular}[b]{@{}p{0.6\textwidth}@{}}
    \centering\includegraphics[width=\linewidth]{\ChapDir/SpectroscopyPipeline.png} \\
    \centering\small (b) Spectroscopy
  \end{tabular}
  \caption[Block diagrams of Photometry \& Spectroscopy pipeline]{Block diagrams of the Photometry and Spectroscopy pipelines. The blocks which have a human face smiley represent blocks which need some human supervision.}
  \label{fig:TIRSPECPhotoSpecPip}
\end{figure}

\subsubsection{Photometry procedure}
The sixth block onwards in Figure \ref{fig:TIRSPECPhotoSpecPip} a) are specific to photometry. The  combined multiple dithered images are aligned (if automatic alignment fails, human supervision will be requested) and combined. Once the reduced images are generated, the pipeline asks the user to select stars of primary  interest, and visually identify good sky regions inside the first image of that source. The pipeline will use triangular matching of the field stars identified using \textit{sextractor} to automatically update the star and sky coordinates for all the subsequent images. These transformed coordinates are used to  do standard aperture photometry, as well as PSF photometry using \textit{daophot} IRAF binaries.
The parameters for all these photometry steps are controlled in the configuration file. Final magnitude outputs of photometry are written to a machine and human readable table.


\subsubsection{Spectroscopy procedure}
The sixth block onwards in Figure \ref{fig:TIRSPECPhotoSpecPip} b) are specific to spectroscopy data reduction.
An optional A-B, B-A subtraction is done if the user demands\footnote{It is not necessary, since TIRSPEC's  slits are quite uniform and HCT does not offer a fast AB dithered observation mode. Hence, taking sky from both sides of the star suffices in case of the TIRSPEC data.}. The spectrum is traced, 1-D extracted, and automatically wavelength calibrated by matching with argon lamp templates.
Once the wavelength-calibrated spectra of the science target as well as a standard star are obtained, one can use the \textit{Telluric Correction Tool} (see below) to remove telluric absorption lines from the spectra.

\subsubsection{Telluric Correction Tool}
Near-infrared spectra contain many telluric lines. They are removed by dividing with the spectrum of a spectroscopic near-infrared standard star, taken within a nearby airmass immediately before or after the target observation. Along with telluric lines, this step also removes the instrument response and outputs  a continuum-corrected spectrum\footnote{Provided the black-body temperature correction of the standard star is known.}. This step also helps in removing the fringes in the \textit{K} band spectrum formed in the HAWAII-1 PACE array used in TIRSPEC. The \textit{Telluric Correction Tool} is a fully interactive graphical  software which removes the intrinsic stellar lines of the standard star, aligns it to the science star, and scales it before dividing the science star spectrum with it. Finally the tool applies black body temperature correction to output a continuum-corrected spectrum of the source. This continuum-corrected spectrum is finally scaled to the magnitude of the source star to obtain the flux-calibrated spectrum of the source. This fully interactive tool includes both, a deconvolution algorithm, as well as a simple Gaussian subtraction algorithm to remove stellar lines of the telluric standards. Figure \ref{fig:TelluricCorrectionPip} shows the paths one can choose through this interactive tool to do telluric correction.

 \begin{figure}[h]
\begin{center}
\includegraphics[width=0.9\textwidth]{\ChapDir/TelluricCorrectionAlgorithm.png} 
\end{center}
\caption[Block diagram of the Telluric Correction Tool]{Block diagram of the fully interactive graphical Telluric Correction Tool. Various paths one could take through the blocks in this tool are represented by the arrows.}
\label{fig:TelluricCorrectionPip}
\end{figure}

\section{Optical Photo-spec Pipeline}
For our \textsc{MFES} program, various optical spectrographs and imagers like HFOSC, IFOSC, and 512$\times$512 ARIES camera were used (see Chapter \ref{Chapter_Obs}). Similar to near-infrared instruments, various optical instruments also have certain shared fundamental steps and a few instrument specific steps in the data reduction procedure. Optical Photo-spec Pipeline was designed by separating the instrument specific algorithms from the fundamental steps. The interface was designed to be same as the near-infrared instrument pipeline. 

The first step in the pipeline is the standardisation of important header information required for reduction across various instruments into a log file\footnote{To be safe, the pipeline will never edit the original data files or folders.}. The sequence of the data reduction routines needed for each instrument is also defined at the beginning. To add support for a new instrument, it is only necessary to update the instrument specific Python class in the source code. The rest of the reduction algorithms are all designed to be independent of the instrument.

After standardisation of the data, depending on the instrument, a selected chain of reduction routines are executed sequentially. The photometry and the spectroscopy steps are conceptually similar to the near-infrared flowchart presented in the previous section \ref{sec:TIRSPECPhotoSpecProcedure}. Similar to the near-infrared pipeline, all the meta data is passed from one block to the next block via human readable text files to provide for  more control in the running of the pipeline.

% the instrument definition concept: refactoring general algirithms
\section{SALT-HRS reduction tool}
\label{sec:SALTHRStool}
We used the High Resolution Spectrograph (HRS) on 11 m SALT for obtaining high resolution (R $\sim$37000 \& $\sim$12000) spectra of southern hemisphere sources in the \textsc{MFES} program. We were the first group to publish results using this instrument, and hence we had to develop our own SALT-HRS reduction tool to reduce the HRS data.

\subsection{Implemented Procedure}
%%% change sentences below or elaborate
In this subsection, I briefly outline the implemented high resolution spectrum extraction procedure. 
Apertures of the echelle frame are first traced by adaptive thresholding of the bright quartz flat spectrum. 
The flux in each aperture is then summed along the axis perpendicular to the dispersion axis. A small tilt of the slit with respect to this axis slightly degrades the resolution of the final collapsed 1-D spectrum. 
In order to perfectly match and align the emission lines in the sky spectrum and the target spectrum of each aperture, the tool scales sky spectrum before subtraction. The scaling factor is estimated using the flux of telluric emission line strengths in the object and the sky spectra. It also shifts along the dispersion axis to correct for any  differential dispersion between the sky and the object spectra. Finally, wavelength calibration is done by identifying ThAr lines. For early epoch observations with HRS, where the ThAr arc spectrum was taken only through the sky fibre, our tool applies the shift correction  obtained using atmospheric emission lines. The wavelength-calibrated spectrum is finally  normalized with  an iteratively fitted smooth continuum.
%%%------

\subsection{Code architecture experiment}
The SALT-HRS reduction tool is written purely in Python, making use of \textit{scikit-image} \citep{vanderWalt14}, \textit{scipy} \citep{jones01}, \textit{numpy} \citep{vanderWalt11} and \textit{astropy} \citep{astropy13}. Its design was an experiment to test the concept of combining the data reduction routines inside the data object itself.
This architecture is fundamentally different from the previous pipelines, where a procedural programming architecture was followed, in which the pipeline is a sequence of routine calls which passes data in fits format as inputs and outputs.

The concept of incorporating data reduction methods into the data object, initially seemed to be an appealing idea to keep all the methods associated with a certain kind of data organised within itself. However, after implementation, I concluded that this architecture is not good for astronomy data reduction applications, for the following reasons.
\begin{itemize}
\item Astronomy data reduction is a chain of procedures, which evolves with time as new algorithms are written for different kinds of observations. However, the format of the data which is generated by the instrument is stable. When the reduction procedure is incorporated into the data object, any minor change in the reduction procedure percolates to a change in the data object, potentially making it incompatible with previous versions of reduced data.
\item In practice, astronomy data pipelines often need to be interrupted and certain procedures bypassed or inserted for special cases. Mixing data and reduction procedure makes this really hard to manage.
\item Since astronomy pipelines often need to be restarted from a certain step, this architecture introduces the unnecessary issues of keeping track of the state of the reduction done inside the data object. Any change in the reduction procedure could easily make the previous partially reduced data incompatible.
\end{itemize}

\section{Status}
All the pipelines developed for various instruments are publicly released under GNU GPL v3+ license. 
Codes are version controlled and hosted on Github pages, and they are being constantly updated and improved
as more and more users provide feedback and suggestions.
Almost all the parts of the pipelines are written in Python, using standard modules used by the  astronomy community, like astropy\footnote{\citet{astropy13}}, numpy\footnote{NumPy is the fundamental package for scientific computing with Python \citep{oliphant07}}, matplotlib\footnote{\citet{hunter07}} and PyRAF\footnote{PyRAF is a product of the Space Telescope Science Institute, which is operated by AURA for NASA}.

The modular implementation with inbuilt documentation designed to be reusable 
%Code structure being build to be reusable at a modular level with inbuilt documentation 
has enabled many other users to use them in their own programs or instrument pipelines.

The TIRSPEC near-infrared imaging and spectroscopy pipeline is heavily used by the user community of TIRSPEC. Several refereed publications have already been published using this pipeline.

The Optical Photo-spec Pipeline is also used by some of the user community of the optical instruments. %(it is not as widely adopted as TIRSPEC, since many users were already comfortable with their own optical reduction procedures before our pipeline was released). 
Due to its versatile architecture, support for many new instrument are being implemented into the pipeline regularly.

The SALT-HRS reduction tool, is also used in several publications. However, it is not being further developed and in the near future the routines from this pipeline will be incorporated into \textsc{pyHRS}, which will be the official reduction tool (currently under development) for the SALT-HRS instrument.

% All the codes are written in modular form with inbuilt documentation, so users can easily modify or create their own pipeline by importing the codes as modules.