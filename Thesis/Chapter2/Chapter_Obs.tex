% Chapter Template

\chapter{Observations \& Data Reduction} % Main chapter title

\label{Chapter_Obs} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

Since the major goal of the \textsc{MFES} program is to study the inner part of the accretion disc, where the instability mechanism that results in episodic accretion occurs, most of our long-term monitoring was done in optical and near-infrared bands. However, we also observed our sources in longer far-infrared and radio wavelengths for constraining their environmental characteristics. For this thesis work, I have done a detailed study of two sources which were most peculiar in our \textsc{MFES} program, namely V899 Mon and V1647 Ori. In this chapter, we shall mainly describe the observations conducted as well as the procedures followed for the data reduction of these two sources.

\section{Optical Photometry} \label{OpticPhoto}
During an FUori or EXori accretion outburst, the temperatures involved in the inner most part of the accretion disc are around $\sim$ 2000 - 3000 K. In these family of outbursts, the light curves in optical bands show maximal increase with respect to the longer wavelength light curves. Hence, to track any periodic as well as non-periodic variations in the flux from the central source, the detailed evolution of accretion rate, extinction, etc.,  we have to monitor the flux from these sources in the optical broad band filters.

Our long-term optical monitoring of V899 Mon started on 2009 November 30, and V1647 Ori on 2008 September 14. The observations were carried out using the  2 m Himalayan \textit{Chandra} Telescope (HCT) at the Indian Astronomical Observatory, Hanle (Ladakh), belonging to the Indian Institute of Astrophysics (IIA), India, and the 2 m telescope at the IUCAA (Inter-University Centre for Astronomy and Astrophysics) Girawali Observatory (IGO), Girawali (Pune), India. For optical photometry, the  central 2K $\times$ 2K CCD section of the Hanle Faint Object Spectrograph \& Camera (HFOSC) with a pixel scale of 0.296\arcsec\,  was used on the HCT, and the 2K $\times$ 2K CCD of the IUCAA Faint Object Spectrograph \& Camera (IFOSC) with a similar pixel scale of 0.3\arcsec\, was used on the IGO\@. This gave us a field of view (FOV) of $\sim 10 \times 10$ arcmin$^{2}$ on both HCT and IGO\@. 
 A detailed description of the instruments and telescopes is available at the IAO\footnote{\url{http://www.iiap.res.in/iao/hfosc.html}} and IGO\footnote{\url{http://www.iucaa.ernet.in/\~itp/igoweb/igo\_tele\_and\_inst.htm}} websites. 
V899 Mon's field $((\alpha,\delta)_{2000} = 06^h09^m19^s.28, -06^{\circ}41\arcmin\,55\arcsec\,.4)$  was observed in standard $UBVRI$ Bessel filters.
We carried out observations for 69 nights, out of which 41 nights were observed through HCT and the remaining ones from IGO\@. The photometric observation log of V899 Mon is given in Table \ref{table:V899MonObs_Log}. V1647 Ori's field $((\alpha,\delta)_{2000} = 05^h46^m13^s.135, -00^{\circ}06^{'}04^{''}.82)$ was also observed with the same filters, and out of our total observation of 110 nights published in our 2013 paper \citep{ninan13}, 84 nights were observed through HCT and 26 nights from IGO\@.
The photometric observation log of V1647 Ori is given in Table \ref{table:V1647OriObs_Log}. %Only a portion of the table is provided here. The complete table is available in machine-readable form in the online journal.

\begin{table}[h]
\begin{center}
\caption{Observation log of V899 Mon}
\begin{tabular}{lccll} 
\toprule
Date & JD & FWHM$^a$ & Filter(s)/Grism(s)& Instrument(s)\\
\midrule
2009 Nov 30 & 2455166 &  1\arcsec.9 & V,R,gr8            & HFOSC	\\
2009 Dec 04 & 2455170 &	 3\arcsec.1 & V,R,I				 & HFOSC	\\
2009 Dec 16 & 2455182 &	 1\arcsec.8 & V,R,I				 & HFOSC	\\
2009 Dec 17 & 2455183 &	    & gr7,gr8				 & HFOSC	\\
\bottomrule
\end{tabular}
\begin{tablenotes}
\item[$^a$] Measured average FWHM. This is a measure of the seeing.
\item Note: This table is published in its entirety in the electronic edition of \citet{ninan15}. A portion is shown here for guidance regarding its form and content.
\end{tablenotes}
\label{table:V899MonObs_Log}
\end{center}
\end{table}


%\begin{deluxetable}{lccll}
% 
%\tabletypesize{\footnotesize}
%\tablecolumns{5} 
%\tablewidth{0pt}
%\tablecaption{  Observation log of V899 Mon \label{table:V899MonObs_Log}}
%\tablehead{ \colhead{Date} & \colhead{JD} & \colhead{FWHM\tablenotemark{a}} & \colhead{Filter(s)/Grism(s)}& \colhead{Instrument(s)}}
%%\hline
%\startdata
%2009 Nov 30 & 2455166 &  1\arcsec.9 & V,R,gr8            & HFOSC	\\
%2009 Dec 04 & 2455170 &	 3\arcsec.1 & V,R,I				 & HFOSC	\\
%2009 Dec 16 & 2455182 &	 1\arcsec.8 & V,R,I				 & HFOSC	\\
%2009 Dec 17 & 2455183 &	    & gr7,gr8				 & HFOSC	\\
%\enddata
%\tablenotetext{a}{Measured average FWHM. This is a measure of the seeing.}
%\tablecomments{This table is published in its entirety in the electronic edition of \citet{ninan15}. A portion is shown here for guidance regarding its form and content.}
%
%\end{deluxetable}

\begin{table}[h]
\begin{center}
\caption{Observation log of V1647 Ori}
\begin{tabular}{lcccc} 
\toprule
Date (UT)  & JD & FWHM$^\dag$ & Filter(s)/grism(s) &  Exposure time (in secs)\\
\midrule
2008 Sep 14 & 2454724 & 1\arcsec.9 & $V,R,I,gr8$   & 300, 480, 240, 2400       \\%     & \\
2008 Sep 15 & 2454724 & 1\arcsec.6 & $gr8$     & 2400               \\
2008 Sep 16 & 2454726 & 1\arcsec.5 & $V,R,I$   & 600, 720, 240       \\%     & \\
2008 Sep 28 & 2454738 & 1\arcsec.1 & $V,R,I$ &  240, 240, 180  \\%     & \\
2008 Oct 01 & 2454741 & 1\arcsec.2 & $V,R,I$   & 120, 120, 90        \\%     & \\
2008 Oct 02 & 2454742 & 1\arcsec.2 & $V,R,I,gr7$ &  240, 240, 180, 2400  \\%     & \\
2008 Oct 03 & 2454743 & 1\arcsec.5 & $gr8$     & 2400               \\
\bottomrule
\end{tabular}
\begin{tablenotes}
\item[$^{\dag}$] Measured average FWHM. This is a measure of the seeing.
\item[$^{\dag\dag}$] Observed from IGO, all other nights are from HCT.
\item Note: This table is published in its entirety in the electronic edition of \citet{ninan13}. A portion is shown here for guidance regarding its form and content.
\end{tablenotes}
\label{table:V1647OriObs_Log}
\end{center}
\end{table}

%\begin{deluxetable}{lcccc}%r}
% 
%\tabletypesize{\footnotesize}
%\tablecolumns{5} 
%\tablewidth{0pt}
%\tablecaption{Observation log of V1647 Ori \label{table:V1647OriObs_Log}}
%\tablehead{ \colhead{Date (UT) } & \colhead{JD} & \colhead{FWHM$^\dag$} & \colhead{Filter(s)/grism(s)} & \colhead{ Exposure time (in secs)} }% &\colhead{Notes}}
%\startdata
%2008 Sep 14 & 2454724 & 1.9\arcsec & $V,R,I,gr8$   & 300, 480, 240, 2400       \\%     & \\
%2008 Sep 15 & 2454724 & 1.6\arcsec & $gr8$     & 2400               \\
%2008 Sep 16 & 2454726 & 1.5\arcsec & $V,R,I$   & 600, 720, 240       \\%     & \\
%2008 Sep 28 & 2454738 & 1.1\arcsec & $V,R,I$ &  240, 240, 180  \\%     & \\
%2008 Oct 01 & 2454741 & 1.2\arcsec & $V,R,I$   & 120, 120, 90        \\%     & \\
%2008 Oct 02 & 2454742 & 1.2\arcsec & $V,R,I,gr7$ &  240, 240, 180, 2400  \\%     & \\
%2008 Oct 03 & 2454743 & 1.5\arcsec & $gr8$     & 2400               \\
%
%
%\enddata
%\tablenotetext{\dag}{Measured average FWHM. This is a measure of the seeing.}
%\tablenotetext{\dag\dag}{Observed from IGO, all other nights are from HCT.}
%\tablecomments{This table is published in its entirety in the electronic edition of \citet{ninan13}. A portion is shown here for guidance regarding its form and content.}
%\end{deluxetable}


The standard photometric data reduction steps like bias subtraction, median flat-fielding, and finally aperture photometry of V899 Mon and V1647 Ori were done using our publicly released pipeline\footnote{\url{https://indiajoe.github.io/OpticalPhotoSpecPipeline/}} for these instruments (see Chapter \ref{Chapter_Pipelines}). 
Blank sky images in \textit{I} band were used to create fringe templates by the \textsc{MKFRINGECOR} task in IRAF\footnote{IRAF is distributed by the National Optical Astronomy Observatories, which are operated by the Association of Universities for Research in Astronomy, Inc., under contract with the National Science Foundation.}, which were later used to subtract the fringes that appeared in \textit{I} band images taken from IGO.

Since V899 Mon source is quite bright and is not affected by any bright nebulosity, we used a 4 $\times$ full width half maximum (FWHM) aperture for optical photometry. The background sky was estimated from a ring outside the aperture radius with a width of 4.5\arcsec\,. 

Point-spread function (PSF) photometry (using PSF and ALLSTAR tasks in the DAOPHOT package of IRAF) on V1647 Ori was not able to fully remove the nebular contamination. We found a strong correlation between fluctuation in magnitude of V1647 Ori and fluctuation in atmospheric seeing condition. This is because the contamination of flux from the nebula into V1647 Ori's aperture was a function of atmospheric seeing. So we generated a set of images by convolving each frame with two-dimensional Gaussian kernels of different standard deviation (using the IMFILTER.GAUSS task in IRAF) for simulating different atmospheric seeing conditions. We then recalculated the magnitudes by DAOPHOT, PSF, and ALLSTAR algorithms of IRAF for various atmospheric seeing conditions. The differential magnitudes obtained from each frame's set were interpolated to obtain magnitude at an atmospheric seeing of 1.18\arcsec\,, which was taken to be the interpolated seeing for all the nights, and was chosen to minimise the interpolation error. This method reduced our error bars in magnitude by a factor of two. Apart from the Gaussian convolution step, the PSF photometry steps were all the same as in \citet{ojha06}.

Magnitudes of the whole McNeil's nebula and other objects in it like region C (near HH 22A) and region B defined by \citet{briceno04} in their Figure 2 (also see Figure \ref{img:opticalfieldV1647Ori}) were measured by simple aperture photometry with an aperture radius of 80\arcsec\, for nebula and 12\arcsec\, for the regions C and B. For obtaining the flux, the aperture of objects like regions C and B was centered at the objects themselves.

Finally, the magnitude calibration was done by solving color transformation equations for each night using nearby Landolt's standard star fields \citep{landolt92}, and also using five and six  secondary standard field stars in V899 Mon and V1647 Ori fields, respectively (see Figure \ref{img:opticalfieldV899Mon} and \ref{img:opticalfieldV1647Ori}). We identified these secondary standard stars based on the stability in their magnitude over the period of our observations.

 \begin{figure*}
 \begin{center}
  \includegraphics[width=0.9\textwidth]{\ChapDir/Iras06068_withSStds.pdf}
 \caption[Optical color composite image of V899 Mon]{RGB color composite optical image of V899 Mon taken with HFOSC  from HCT ($V$: blue; $R$: green; $I$: red) on 2011 January 2. V899 Mon was in its short-duration quiescent phase at this epoch. The FOV is $\sim$ 10 $\times$ 10 arcmin$^2$. North is up and east is to the left-hand side. Stars marked as A, B, C, D, and E are the secondary standard stars used for magnitude calibration. The location of V899 Mon is marked with two perpendicular lines at the middle.}
 \label{img:opticalfieldV899Mon}
 \end{center}
 \end{figure*}
 \begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{\ChapDir/McNeil_RGB4paper.eps}%{McNeil_RGB4paper.eps}
\caption[Optical color composite image of McNeil's nebula (V1647 Ori)]{Color composite image of McNeil's nebula (V1647 Ori) region taken from IGO (\textit{V}: blue; \textit{R}: green; \textit{I}: red) on 2010 February 13. FOV is $\sim 10 \times 10$ arcmin$^2$. North is up and east is to the left-hand side. Stars marked as A, B, C, and D are the secondary standard stars used for magnitude calibration. The location of V1647 Ori is marked at the center by two perpendicular lines. Region C, overlapping Herbig--Haro object HH 22A, is marked together. A knot in the southwestern section (region B) of the nebula is also marked.  At 400 pc distance, the scale 1\arcmin\,  corresponds to 24,000 AU\@.}
\label{img:opticalfieldV1647Ori}
\end{center}
\end{figure} 
 
\section{NIR Photometry} \label{NirPhoto}
Near-infrared (NIR) fluxes are also dominated by the energy released from heavy accretion. Unlike in optical bands, NIR bands are less affected by  the extinction, and they can probe the extinct central source better than the optical bands. Due to the heavy extinction towards some of the sources in our \textsc{MFES} program, their central source was visible only in our NIR observations.

NIR photometric monitoring of our sources in \textit{J, H,} and \textit{K / K$_S$} bands was carried out using the HCT NIR camera (NIRCAM), the TIFR Near Infrared Spectrometer and Imager (TIRSPEC) mounted on HCT, and the TIFR Near Infrared Imaging Camera-II (TIRCAM2) mounted on the IGO telescope as well as the 1.2 m Physical Research Laboratory (PRL) Mount Abu telescope. NIRCAM has a $512 \times 512$ mercury cadmium telluride (HgCdTe) array, with a pixel size of 18 $\mu m$, which gives an FOV of $\sim$ $3.6 \times 3.6$ arcmin$^{2}$ on HCT\@. Filters used for observations were $J$ ($\lambda_{center}$= 1.28 $\mu m$, $\Delta\lambda$= 0.28 $\mu m$), $H$ ($\lambda_{center}$= 1.66 $\mu m$, $\Delta\lambda$= 0.33 $\mu m$), and $K$ ($\lambda_{center}$= 2.22 $\mu m$, $\Delta\lambda$= 0.38 $\mu m$). Further details of the instrument are available at \url{http://www.iiap.res.in/iao/nir.html}. TIRSPEC has a $1024 \times 1024$ Hawaii-1 PACE\footnote{HgCdTe Astronomy Wide Area Infrared Imager -1, Producible Alternative to CdTe for Epitaxy} (HgCdTe) array, with a pixel size of 18 $\mu m$, which gives an FOV of $\sim$ $5 \times 5$ arcmin$^{2}$ on HCT\@. Filters used for observations were $J$ ($\lambda_{center}$= 1.25 $\mu m$, $\Delta\lambda$= 0.16 $\mu m$), $H$ ($\lambda_{center}$= 1.635 $\mu m$, $\Delta\lambda$= 0.29 $\mu m$), and $K_S$ ($\lambda_{center}$= 2.145 $\mu m$, $\Delta\lambda$= 0.31 $\mu m$) (Mauna Kea Observatories near-infrared filter system). Further details on TIRSPEC are available in Chapter \ref{Chapter_tirspec} and \citet{ninan14,ojha12}. 
TIRCAM2 has a $512 \times 512$ indium antimonide (InSb) array with a pixel size of 27 $\mu m$. 
Filters used for observations were $J$ ($\lambda_{center}$= 1.20 $\mu m$, $\Delta\lambda$= 0.36 $\mu m$), $H$ ($\lambda_{center}$= 1.66 $\mu m$, $\Delta\lambda$= 0.30 $\mu m$), and $K$ ($\lambda_{center}$= 2.19 $\mu m$, $\Delta\lambda$= 0.40 $\mu m$). More details on TIRCAM2 are available in Chapter \ref{Chapter_TFIRBT} and \citet{naik12}. The log of NIR observations of V899 Mon and V1647 Ori is listed in Table \ref{table:V899MonObs_Log} and Table \ref{table:V1647OriObs_Log}, respectively. For V899 Mon, we have photometric observations for a total of 25 nights in the NIR bands. Our first \textit{JHK} observation was carried out during the peak of V899 Mon's first outburst on 2010 February 20, and the remaining observations were carried out during its current ongoing second outburst. For V1647 Ori, in the paper published in 2013 \citep{ninan13}, we have a total of 14 nights of NIR photometric observations, with the first set of data  taken  during the quiescent phase in 2007, i.e. before its 2008 outburst.

 NIR observations were carried out by the standard telescope dithering procedure. Five dither positions were observed. 
 All the dithered object frames were median combined to generate master sky frames for NIRCAM and TIRCAM2. We combined twilight flats and all non-extended source frames observed during the same night to create accurate flats for each night. For NIRCAM and TIRCAM2, data reduction and final photometry were performed using the standard tasks in IRAF, while the TIRSPEC data were reduced using our TIRSPEC photometry pipeline \citep{ninan14} (see Chapter \ref{Chapter_Pipelines}). 
For aperture photometry we used an aperture of 3$\times$FWHM and the background sky was estimated from an annular ring outside the aperture radius with a width of 4.5\arcsec .
 Magnitude calibration was done by solving color transformation equations for each night using Hunt's standard star fields AS 13 and AS 9 \citep{hunt98}, and the five field stars in case of V899 Mon (labeled in Figure \ref{img:nirfieldV899Mon}) that we identified to have a stable magnitude and that are consistent with 2MASS\footnote{Two Micron All Sky Survey} over the period of our observations. For V1647 Ori, on 2011 December 6, standard stars were not observed; hence, we used the magnitude of the nearby star (2MASS J05461162-0006279)  measured on other nights for photometric calibration.

 \begin{figure*}
 \begin{center}
  \includegraphics[width=0.9\textwidth]{\ChapDir/V899Mon_RGBJHK_SStd.pdf}
 \caption[NIR color composite image of V899 Mon]{RGB color composite NIR image of V899 Mon taken with TIRSPEC from  HCT ($J$: blue; $H$: green; $K_S$: red) on 2013 November 14. V899 Mon was in its second outburst phase at this epoch. The FOV is $\sim$ 5 $\times$ 5 arcmin$^2$. North is up and east is to the left-hand side. Stars marked as A, B, C, D, and E are the secondary standard stars used for magnitude calibration. The location of V899 Mon is marked with two perpendicular lines at the middle.}
 \label{img:nirfieldV899Mon}
 \end{center}
 \end{figure*}


\section{Medium-resolution Optical Spectroscopy} \label{OpticSpec}
For detailed measurements of the kinematics of outflows as well as the environmental characteristics like temperature, density, etc. we need to study the time evolution in spectral lines.

Our medium-resolution ($R \sim 1000$) optical spectroscopic monitoring of V899 Mon started on 2009 November 30, and V1647 Ori on 2008 September 14. The spectroscopic observations were carried out using both HCT and IGO. The full 2K $\times$ 4K section of the HFOSC CCD spectrograph  was used for HCT observations, and the 2K $\times$ 2K IFOSC CCD spectrograph was used for IGO observations.
These observations were done in the effective wavelength range of $3700 - 9000$  $\mathring{A}$, using grism 7 (center wavelength 5300 $\mathring{A}$) and grism 8 (center wavelength 7200 $\mathring{A}$). The spectral resolution obtained for grism 7 and grism 8 with the 150 $\mu m$ slit at IGO and the 167 $\mu m$ slit at HCT was $\sim 7$ $\mathring{A}$.
The log of spectroscopic observations of V899 Mon and V1647 Ori is listed in Table \ref{table:V899MonObs_Log} and Table \ref{table:V1647OriObs_Log}, respectively. For V899 Mon, we have 8 spectra from its first outburst phase, 4 from its short quiescent phase, and 30 from its ongoing second outburst phase. For V1647 Ori, in our 2013 paper \citet{ninan13}, we carried out spectroscopic observations for 35 nights through HCT and 24 nights from IGO, thus totaling 59 nights of spectroscopic observations of V1647 Ori's ongoing outburst.

Nebulosity contamination in the spectrum of V1647 Ori was minimised by keeping the slit in the east-west orientation.
For wavelength calibration, we have used  FeNe, FeAr, and HeCu lamps. Standard IRAF tasks like \textsc{APALL} and \textsc{APSUM} were used for spectral reduction using our publicly released \textsc{PyRAF}\footnote{\textsc{PyRAF} is a product of the Space Telescope Science Institute, which is operated by AURA for NASA}-based pipeline developed for both HFOSC and IFOSC instruments (see Chapter \ref{Chapter_Pipelines}). 

\section{High-resolution Optical Spectroscopy} \label{HRSSpec}

We acquired multi-epoch high-resolution ($R \sim$ 37,000) spectra of V899 Mon during its second outburst phase on 2014 December 22, 2015 December 15, 19, and 2016 February 20, using the Southern African Large Telescope
High Resolution Spectrograph (SALT-HRS) \citep{bramall10}. Both the red arm ($5490 - 8810$ $\mathring{A}$) and the blue arm ($3674 - 5490$ $\mathring{A}$) of the HRS instrument were used. %, to take a single exposure of 3170 seconds. 
For this medium-resolution instrument mode of HRS, it uses a 2.23\arcsec\, fiber to collect light from the target source, and another fiber of the same diameter is used to sample a nearby patch of the sky. Apart from the target frames, ThAr arc lamp spectrum was also taken through the sky fiber for wavelength calibration. For tracing the aperture of target and sky fiber, we used bright quartz continuum flat taken through target fiber and sky fiber on nearby nights.

Since our total exposure was taken in a single readout, we had to carefully remove many cosmic rays hits in each frame. We used \textit{Laplacian Cosmic Ray Identification} by \citet{dokkum01} to clean cosmic rays hits in the data frames. 
A reduction tool for SALT-HRS data was written and used to obtain wavelength calibrated, normalised spectrum (see section \ref{sec:SALTHRStool} in Chapter \ref{Chapter_Pipelines} for the reduction procedure I implemented in this tool).
%Apertures in the echelle frame were traced by adaptive thresholding of the quartz flat spectrum. Flux in each aperture was summed along the axis perpendicular to the dispersion axis. A small tilt of the slit with respect to this axis slightly degraded the resolution of the final spectrum. In order to perfectly match and align the emission lines in sky spectrum and target spectrum of each aperture, we had to scale the sky spectrum by a multiplicative factor ($\sim 1.2$) and also shift along the dispersion axis. Finally, wavelength calibration was done by identifying Thorium Argon (ThAr) lines, and we applied the correction shift obtained from atmospheric emission lines for early epoch observations where the ThAr arc spectrum was taken through sky fiber. Wavelength-calibrated spectrum was normalized using a smoothly fitted continuum.

%%ll the SALT-HRS data reduction was done by writing a reduction tool in Python,  (more on this reduction tool in Chapter ). 
The additive factor to translate observed velocities in the spectrum to heliocentric velocities was found to be 0.92 km $s^{-1}$, 4.22 km $s^{-1}$, 2.39 km $s^{-1}$, and -22.31 km $s^{-1}$ for our four nights of observation using the \textit{rvcorrect} task in IRAF.


\section{NIR Spectroscopy} \label{NirSpec}
NIR (1.02 -- 2.35 $\mu m$) spectroscopic monitoring of V899 Mon started on 2013 September 25 using TIRSPEC mounted on HCT. Depending on the  seeing conditions, slits with 1.48$\arcsec$ or 1.97$\arcsec$ widths were used. 
Spectra were taken in at least two dithered positions inside the slit. Spectra of an argon lamp for wavelength calibration and a tungsten lamp for the continuum flat were also taken immediately after each observation without moving any of the filter wheels. For telluric line correction, bright NIR spectroscopic standard stars within a few degrees and similar airmasses were observed immediately after observing the source. The typical spectral resolution obtained in our spectra is R $\sim$ 1200.
The log of spectroscopic observations of V899 Mon is listed in Table \ref{table:V899MonObs_Log}.  It may be noted that all the 12 NIR spectral observations are taken during the  current ongoing second outburst phase of V899 Mon.

NIR spectral data were reduced using the TIRSPEC pipeline \citep{ninan14} (see Chapter \ref{Chapter_Pipelines}). After wavelength calibration, the spectrum  of V899 Mon was divided by a standard star spectrum to remove telluric lines and detector fringes seen in $H$ and $K$ orders' spectra. 
This continuum-corrected spectrum was scaled using the flux estimated from photometry to obtain the flux-calibrated spectrum. Since we do not have $Y$ band photometry, we interpolated $I$ band and $J$ band $\lambda f_\lambda$ flux to $Y$ band and used the interpolated flux to scale the $Y$ band spectrum flux.


\section{GMRT Radio Continuum Imaging} \label{GmrtImag}
The detection of any free-free emission in radio band will provide estimates on the extent of the \HII region due to accretion or outflow shocks, as well as the Lyman continuum flux.
Continuum interferometric observation of V899 Mon at 1280 MHz with 33.3 MHz bandwidth was carried out on 2014 October 17 using the Giant Metrewave Radio Telescope (GMRT), Pune, India. GMRT consists of 30 dish antennae (each 45 m in diameter) in hybrid ``Y'' configuration \citep{swarup91}. Out of these, 25 antennae were online during our observation.  The standard flux calibrator 3C 147 was observed for 15 minutes at the beginning and end of the observations. For phase calibration, the nearby Very Large Array calibrator 0607-085 was observed for 10 minutes after every 20 minutes of integration on V899 Mon. The total integration on the V899 Mon source was 4.5 hr.

\textit{CASAPY} software was used for the data reduction. After careful iterative bad data flagging and gain calibration, 
 we imaged the large FOV of 28'$\times$28' field by dividing the field into 128 w-projection planes using the \textit{CLEAN} algorithm.

\section{\textit{Herschel} Far-infrared Photometry} \label{HerschelPhoto}
The far-infrared flux in YSOs is dominated by extended outer disc and left over envelopes. Hence, they do not vary over short timescales like optical or NIR. But, far-infrared fluxes are very important for estimating the properties of the envelope, age, existence of outflow cavity, etc.

We obtained imaging observations of the V899 Mon region in SPIRE\footnote{Spectral and Photometric Imaging Receiver} 250, 350, and 500 $\mu m$ and PACS\footnote{Photoconductor Array Camera and Spectrometer} 70 and 160 $\mu m$ from the \textit{Herschel} data archive.\footnote{\textit{Herschel} is an ESA space observatory with science instruments provided by European-led Principal Investigator consortia and with important participation from NASA.} SPIRE data were available for two epochs, one on 2010 September 4 (Proposal ID: KPGT\_fmotte\_1) and another on 2013 March 16 (Proposal ID: OT1\_rgutermu\_1). PACS data of V899 Mon were only available in the later epoch. Data from both the epochs were reduced using the standard \textit{Herschel}'s HIPE pipeline with the latest calibration file. For point-source aperture photometry of V899 Mon, we first tried the {\it sourceExtractorSussextractor} routine in HIPE on level 2 SPIRE data and the {\it multiplePointSourceAperturePhotometry} routine on PACS data. We also tried 
 point-spread function (PSF) photometry using the \textit{daophot} tool in IRAF. 
 Since all these algorithms use an annular ring for background estimation and the background is highly non-uniform, the results were found to be very sensitive to aperture and background estimates. We finally 
estimated fluxes using \textit{getsources} \citep{menshchikov12}, which uses a linearly interpolated background estimate. These estimates were found to be 
consistent with the differential flux between two epochs (differential flux could be estimated more accurately since the subtracted image has a uniform background). The HIPE pipeline assumes a spectral energy distribution (SED) color,  $\alpha=-1$ (where $F_{\nu} = \nu^{\alpha}$). The actual color $\alpha$ of V899 Mon was obtained from our flux estimates, and the corresponding color correction factor was applied to the flux values for obtaining color-corrected flux estimates.

\section{WISE Photometry} \label{WISEPhoto}
We obtained imaging observations of the V899 Mon region in $W1$ (3.4 $\mu m$), $W2$ (4.6 $\mu m$), $W3$ (12 $\mu m$), and $W4$ (22 $\mu m$) bands from the \textit{Wide-field Infrared Survey Explorer (WISE)} data archive \citep{wright10}. Photometric magnitude estimates were also available in the \textit{WISE} all-sky catalog. $W1$ and $W2$ bands were observed at two epochs on 2010 March 17 and 2010 September 24, whereas $W3$ and $W4$ were observed only on 2010 March 17. 
 Since $W1$ and $W2$ intensity maps looked severely saturated, we have used only $W3$ and $W4$ \textit{WISE} catalog magnitudes for our study of V899 Mon.
~\\~\\

For the study of our sources in the \textsc{MFES} program, we have also compiled other publicly available archival data of our source region at different epochs. Data were obtained from the following surveys and instruments: CRTS DR2\footnote{Catalina Real-Time Transient Survey Data Release 2},  POSS survey\footnote{Palomar Observatory Sky Survey}, USNO-B catalog\footnote{U.S. Naval Observatory B1.0 Catalog}, IRAS survey\footnote{Infrared Astronomical Satellite, Survey}, DENIS survey\footnote{Deep Near Infrared Survey of the Southern Sky}, 2MASS survey\footnote{Two Micron All-Sky Survey}, AKARI\footnote{Akari All-Sky Survey Point Source Catalog}, IRAC\footnote{Infrared Array Camera on Spitzer} and MIPS\footnote{Multi-Band Imaging Photometer on Spitzer}.




