% Chapter Template

\chapter{Conclusion} % Main chapter title

\label{Chapter_Conclusion} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

As part of my doctoral thesis work, I carried out a long-term ~multi-wavelength monitoring of FUors and EXors Sources (\textsc{MFES} program). The observations were carried out in optical, near-infrared, and radio wavelengths using primarily the 2 m class telescopes in India (HCT and IGO Telescope), 11 m SALT in South Africa, and GMRT in India. The data included hundreds of nights of observations spanning from 2008 to 2016. 

In this chapter, the major results from this thesis work are summarised.

\section{Part I : On episodic accretion and Outflows from YSOs}

\begin{description}
\item [Bimodal classification of FUors and EXors is probably unreal]\hfill \\
Our detailed characterisation of V899 Mon as well as V1647 Ori showed both these sources demonstrated a mixture of properties seen in classical FUors and EXors. Both these sources are still undergoing outburst and their timescales are more than EXors but less than classical FUors. Spectroscopically, they are more like EXors but a few characteristics are similar to FUors.  Hence, we propose V1647 Ori and V899 Mon as a new intermediate class of eruptive variables. This family of episodic accretion outbursts probably has a “continuum” distribution and is not bimodal in nature.

\item [Transition to short period quiescence between two outbursts of V899 Mon]\hfill \\
Due to our continuous monitoring MFES program, for the first time in the family of eruptive variables, we could spectroscopically monitor the transition from the first outburst to a short period quiescence and back to the second outburst of V899 Mon. This gave significant insights on the temperature variations as well as the evolution of outflows during the transition phases of eruptive variables. We identified that the flux was reddest during the transition phase, which indicates the flux at the onset of the outburst was cooler than the stellar color temperature during the quiescent phase.

\item [Inadequacy of disc instability models] \hfill \\
We showed the long, second outburst seen in both V1647 Ori and V899 Mon cannot be explained by the `$S$-curve' disc instability models like the thermal and magnetorotational instability models. Using the $\alpha$-disc parametrisation of viscous disc, we proposed the likely scenarios in the inner disc region which could explain the observed time scales in the light curves.

\item [Instability in magnetospheric accretion as the cause of short breaks]\hfill \\
The disc instability models cannot explain the short period quiescence we observed in V1647 Ori and V899 Mon. Based on the time scales of the rise time and the duration of the second outburst, we showed that the mechanism needed to explain the short break between the first and the second outburst should be capable of temporarily pausing the accretion without fully draining the inner disc below a critical density (like the disc instability models do).
The heavy outflows and large turbulence detected just before V899 Mon transitioned to quiescent phase seemed to indicate a highly dynamic unstable activity in the magnetospheric accretion. Comparing with the simulations of the instability in the magnetospheric accretion in the literature, we proposed this as the most consistent mechanism which can explain all the traits we observed during the transition phases. These mechanisms can pause the accretion temporarily without having to drain the disc and can easily resume as soon as the magnetic fields stabilise with a sharp rise in the light curve. 

\item [Direct detection of accretion - outflow correlation]\hfill \\
From our V899 Mon observations, for the first time we could directly detect the outflows, traced by the P-Cygni profiles of H$\alpha$ and Ca II IR triplet lines, disappearing below the detection limit when the accretion rate dropped during the quiescent phase. We also detected resumption of the outflows during the second accretion outburst. This confirms the accretion driven outflow models.

\item [Detection of episodic magnetic reconnection winds in V1647 Ori]\hfill \\
We detected short duration episodic (few weeks timescale) outflow winds in V1647 Ori, which is at an inclination angle of $\sim$ 61$^o$. From our analysis, we proposed that these are likely to be magnetic reconnection winds.

\item [First detection of magnetically accelerated polar winds] \hfill \\
Our high resolution multi-epoch spectra of V899 Mon showed fast episodic winds in timescales of a few days. Such variations have never been reported in any young stars. Our analysis showed magnetically accelerated polar winds seen in simulations by \citet{kurosawa12,romanova09}, which is a weak field case of the propeller-regime, is the most likely outflow driving mechanism. They can have the large velocity changes and the timescales we detected in the outflow from V899 Mon.

\item [Hierarchical Bayesian model to constrain outburst frequency] \hfill \\
We developed a framework for a powerful, flexible hierarchical  Bayesian model which can be used to constrain the period of FUors and EXors outbursts using all the available multi-epoch, multi-wavelength photometric data of YSOs in the literature. Our simulations showed the model is powerful enough to constrain the outburst frequency much more than simple binomial model methods, and that they can also give quantitative probability on comparison between the outburst frequencies across different classes of YSOs. This is extremely fundamental to the study of the evolution of outbursts and its influence on the protoplanetary disc as the system evolves. This model also enables us to constrain the decay timescale of FUors outbursts.

\end{description}

\section{Part II : Instrumentation}

In this section, the current status of the instruments and pipelines which were developed as part of this thesis are summarised.

\begin{description}
\item [TIFR Near Infrared Spectrometer and Imager (TIRSPEC)]\hfill \\
After taking part in the development, I conducted repairs, upgrades and carried out detailed calibrations of TIRSPEC on HCT. The instrument has been released for astronomical observations from May 2014. It is currently heavily used by the community on 2 m HCT. It is the first public near-infrared spectrograph in the country. Several science results from local star formation to extra galactic studies have already been published by various groups using my calibrations and pipelines. 

\item [TIFR Near Infrared Imaging Camera - II (TIRCAM2)] \hfill \\
After carrying out calibration of the upgraded TIRCAM2, it was extensively used for science observations from 2 m IGO telescope. Recently we have shifted TIRCAM2 to the largest telescope in India, the 3.6 m DOT in Nainital. TIRCAM2 is the only camera in the country which can image in the L' band (3.6 $\mu m$).

\item [100 cm TIFR Far-Infrared Balloon-borne Telescope]\hfill \\
The wavelet filter signal processing I developed has enabled us to improve the pointing and aspect of the 100 cm TIFR Far-Infrared Balloon-borne Telescope while operating in fast un-chopped mode. We will be having our next balloon flight by the end of 2016. The pipeline for the FIR Fabry-Perot spectrometer on this telescope is also successfully implemented and it generates clean maps of 158 $\mu m$ [C II] line emission as well as continuum.

\item [Unified NIR and Optical Pipelines for multiple instruments]\hfill \\
By separating out the instrument specific parts from the general algorithms of optical and near infrared data reduction,  I developed two modular pipelines for NIR instruments and Optical instruments for reducing both photometry as well as spectroscopy data. These pipelines currently support more than five different instruments, and more instruments are incorporated as they are commissioned. Several science results have already been published in refereed journals by various groups using these pipelines. 
\end{description}

\section{Future work}
We have demonstrated that the FUors and EXor systems are the best laboratories to test any accretion-outflow models. While it is possible to measure instantaneous outflows by carrying out high resolution optical spectroscopy, it is difficult to estimate the instantaneous accretion rates by using the line fluxes alone. Simultaneous long term X-ray observations along with high-resolution optical and near-infrared spectroscopy will give a better understanding of the relationship between the accretion and the outflow mechanisms.

Continuing the long-term multi-wavelength monitoring of all the available FUors and EXors will provide more insights on the environmental effects of these episodic outbursts on the protoplanetary discs. Modelling the effect of the outbursts on the chemistry of the disc, snow-line, dust structure, etc., needs to be carried out for each source and can in principle be verified by conducting observations with ALMA\footnote{Atacama Large Millimeter Array}. 

The hierarchical Bayesian model, we have developed, shows very promising results in our simulations. Codes written for the model are directly applicable to the real data. Hence, the next step is to compile all the available multi-epoch, multi-wavelength photometric observations of the YSOs and to fit the model to them.

On the instrumentation side, lots of new exciting instruments are being developed in the country. The expertise from TIRSPEC, TIRCAM2, etc, will be invaluable for the next generation instruments we are currently developing at TIFR, namely TANSPEC\footnote{TIFR-ARIES Near Infrared Spectrograph} and IRSIS\footnote{Infrared Spectroscopic Imaging Survey (satellite)}.
TIRSPEC can also be upgraded to the more modern H1RG detector array to keep the instrument competitive for the next decade.